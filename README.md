# INF226 Exam Score Checker

[![';DROP TABLE students;--](https://imgs.xkcd.com/comics/exploits_of_a_mom.png)](https://xkcd.com/327/)


## To use
You need to install these Python packages first – [Flask](https://flask.palletsprojects.com/en/2.2.x/), [APSW](https://rogerbinns.github.io/apsw/), [Pygments](https://pygments.org/):

```shell
pip install flask
pip install apsw
pip install pygments
```

## Start the Web Server
Use the `flask` command to start the web server:

```shell
$ cd gradinator
$ flask run
```

If the `flask` command doesn't exist, you can start use `python -m flask run` instead.

```shell
$ python -m flask run
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:5000
Press CTRL+C to quit
```

Assuming it works, you should find the web server in your browser at http://localhost:5000/ 

(For server apps that aren't called `app.py`, you can add the `--app` option: `flask --app hello.py run`)

**WARNING:** *Don't expose this example server to the internet – it's horribly insecure!*

## Database

You don't have to set up a database server or anything – we're using [SQLite](https://sqlite.org/index.html), which is embedded in the program (it's bundled with the APSW library, so you don't have to install anything).

Data is loaded from `resultsdb.json` (not included). All the SQL stuff is done with SQLite's `:memory:` database, with a fresh setup for each client.

